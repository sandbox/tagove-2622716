// $Id: README.txt,v 1.0 2015/11/25 12:31:40 $

### ABOUT

  This module adds the necessary script to the footer of ones site for prompting users to chat via tagove.

  Current Features:
      * Administration settings to allow setting your account number for the script
      * Setting the pages in which to show the script:
            o From a blacklist of pages
            o From a whitelist of pages
            o By returning a value of true or false from PHP snippet
      * Setting visibility of script by role

### INSTALLING

  1. Extract tagove tarball into your sites/all/modules directory so it looks like sites/all/modules/tagove  2. Navigate to Administration -> Modules and enable the module.
  3. Navigate to Administration -> Configuration -> System -> Tagove and add email and password as well as any other configuration options you want.

### CREDITS

  Originally developed by Tagove Team.
