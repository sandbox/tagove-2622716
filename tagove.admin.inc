<?php
// $Id: tagove.admin.inc,v 1.2.2.3 2015/11/18 14:35:27 tagove Exp $

/**
 * @file
 * Administrative page callbacks for the Tagove module.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 *
 * @param array $form_state
 *   structure associative drupal form array
 * @return array
 */
define('TAGOVE_URL','app.tagove.com');

function tagove_admin_settings_form() {

        $form['tagove'] = array(
            '#type' => ''
        );
    if(variable_get('chat_widget')) {
        $form['desc'] = array(
            '#type' => 'fieldset',
            '#title' => t('Setup Your Tagove Account'),
            '#collapsible' => TRUE,
            '#group' => 'tagove',
            '#description' => '<p>' . t('Currently Activate Account.') . '</p>' .
                              '<p>' . t('To start Togove Chat, Launch our dashboard for access all feature including widget customization.') . '</p>'
        );
        $form['logged'] = array(
            '#type' => 'submit',
            '#title' => t('Logout'),
            '#collapsible' => FALSE,
            '#value' => 'Logout',
            '#name' => 'remove_tagove',
            '#group' => 'tagove',
        );
        $form['my_button'] = array(
            '#type' => 'submit',
            '#value' => t('Launch Dashboard'),
            '#name'=>'my_button',
            '#target'=>'_blank',
        );

        drupal_add_js("jQuery(document).ready(function () { jQuery('#edit-submit').css('display','none'); });", 'inline');
        drupal_add_js('jQuery(document).ready(function(){
                       jQuery("#edit-my-button").click(function(){
                        window.open("http://app.tagove.com/user/login/");
                        return false;
                     });
                   });', 'inline', 'footer');
    }

   else {
       // General Settings
       $form['account'] = array(
           '#type' => 'fieldset',
           '#title' => t('Linked Up With Tagove Account'),
           '#collapsible' => FALSE,
           '#group' => 'tagove',
       );
       $form['account']['tagove_email'] = array(
           '#type' => 'textfield',
           '#title' => t('Email'),
           '#default_value' => variable_get('tagove_email', ''),
           '#size' => 40,
           '#maxlength' => 40,
           '#required' => TRUE,
       );
       $form['account']['tagove_password'] = array(
           '#type' => 'password',
           '#title' => t('Password'),
           '#default_value' => variable_get('tagove_password', ''),
           '#size' => 40,
           '#maxlength' => 40,
           '#required' => TRUE,
           '#description' => '<p>' . t('The email is unique to the websites domain and can be found in the script given to you by the Tagove dashboard settings.') . '</p>' .
               '<p>' . t('Go to !url, login.', array('!url' => l('app.tagove.com/user/login', 'http://app.tagove.com/user/login', array('attibutes' => array('target' => '_blank'))))) . '</p>'

       );
   }
        return system_settings_form($form);

  // END General Settings

}
/**
 * Implementation of hook_admin_settings_form_validate().
 *
 * @param array $form
 *   structured associative drupal form array.
 * @param array $form_state
 */
function tagove_admin_settings_form_validate($form, &$form_state) {
    if(isset($_REQUEST["remove_tagove"])){
        variable_del('chat_widget');
        drupal_goto('admin/config/system/tagove');
    }
    if(isset($_REQUEST ["my_button"])){
        drupal_goto('http://app.tagove.com/user/login/');
    }
    if (empty($form_state['values']['tagove_email']) && empty($form_state['values']['tagove_password'])) {
        form_set_error('tagove_email', t('A valid Tagove email is needed.'));
        form_set_error('tagove_password', t('A valid Tagove password is needed.'));
    }

  else
  {

    /*$url = 'https://'.TAGOVE_URL.'/user/login';
    $response = drupal_http_request( $url, array(
            'method' => 'POST',
            'body' => array('api'=> 'true','Login'=>'Login', 'Email' => $form_state['values']['tagove_email'], 'Password' => $form_state['values']['tagove_password'] ),
        )
    );*/

    $options = array();
    // Array keys are matching the key that the remote site accepts. URL encoding will be taken care later.
    $options['data'] = array(
        'api'=> 'true','v2'=>'true','Login'=>'Login', 'Email' => $form_state['values']['tagove_email'], 'Password' => $form_state['values']['tagove_password'],
    );
    $options['data'] = http_build_query($options['data']);
    $options['method'] = 'POST';
    $options['headers'] = array('Content-Type' => 'application/x-www-form-urlencoded');

    // Array keys are matching the key that the remote site accepts. URL encoding will be taken care later.
    $url = 'http://'.TAGOVE_URL.'/user/login';
    $response = drupal_http_request($url, $options);
    /*print_r($options);*/
    $rsult =  get_object_vars($response);
    $result= $rsult['data'];
    if(strpos($result,'error')!==false){
        form_set_error('tagove_email', t('A valid Tagove email is needed.'));
        form_set_error('tagove_password', t('A valid Tagove password is needed.'));
    } else {
        variable_set( "chat_widget", $result);

    }

  }
    function tagove_submit_handler(&$form, &$form_state) {
        $form_state['redirect'] = 'home';
    }
}
